package com.pushdown.automaton.exceptions;

import com.pushdown.automaton.exceptions.codigos.CodigoError;

public class DatosNoValidosException extends Exception {
	public static final String MAL_FORMADA = ", no esta bien formada";
	private final transient CodigoError codigoError;
	private static final long serialVersionUID = -7979803280598637482L;
	
	public DatosNoValidosException(final CodigoError tipoError){
		super(tipoError.getDescripcion());
		this.codigoError = tipoError;
	}
	
	public DatosNoValidosException(String mensaje, CodigoError tipoError){
		super(mensaje);
		this.codigoError = tipoError;
	}
	
	public CodigoError getCodigoError(){
		return codigoError;
	}
}
