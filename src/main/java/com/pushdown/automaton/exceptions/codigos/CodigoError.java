package com.pushdown.automaton.exceptions.codigos;

public interface CodigoError {
	public String getDescripcion();
}
