package com.pushdown.automaton;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.bind.annotation.RequestMethod;

import com.pushdown.automaton.controller.AutomataController;
import com.pushdown.automaton.model.AutomataPila;
import com.pushdown.automaton.utils.Utils;

@RunWith(SpringRunner.class)
@SpringBootTest (webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Test_AutomataController {
	
	@Autowired
	AutomataController automataController;
	
	private static final String EJEMPLO_BODY_GENERAR = "{a,b};{S,N};{q0,q1};q0;S;\nf(q0,a,S)=(q0,N)\nf(q0,a,N)=(q0,NN)\nf(q0,b,N)=(q1, )\nf(q1,b,N)=(q1, )";
	private int port = 7795;
	
    @Autowired
    private TestRestTemplate template;
    
	HttpHeaders headers = new HttpHeaders();

	@Test
	public void CP1_1_TipoPeticionErroneaEndPointGeneracion() {
		ResponseEntity<String> respuesta = template.getForEntity(createURLWithPort("/generate"),
                String.class);
		
		assertEquals(HttpStatus.METHOD_NOT_ALLOWED, respuesta.getStatusCode());
	}
	
	@Test
	public void CP1_2_TipoPeticionErroneaEndPointValidacion() {
		ResponseEntity<String> respuesta = template.postForEntity(createURLWithPort("/checkword/1/1"),
                String.class, null, headers);
		
		assertEquals(HttpStatus.METHOD_NOT_ALLOWED, respuesta.getStatusCode());
	}
	
	@Test
	public void CP2_1_Generacion_sin_body() {
		ResponseEntity<String> respuesta = template.postForEntity(createURLWithPort("/generate"),
                null, null, headers);
		
		assertEquals(HttpStatus.BAD_REQUEST, respuesta.getStatusCode());
	}
	
	@Test
	public void CP2_2_RutaGetMalFormada_404_sinIdAutomata() {
		
		ResponseEntity<String> respuesta = template.getForEntity(createURLWithPort("/checkword//basa"),
                null);
		assertEquals(HttpStatus.NOT_FOUND, respuesta.getStatusCode());
	}
	
	@Test
	public void CP2_3_RutaGetMalFormada_404_sinPalabra() {
		
		ResponseEntity<String> respuesta = template.getForEntity(createURLWithPort("/checkword/1/ "),
				String.class, 1, " ");
		assertEquals(HttpStatus.NOT_FOUND, respuesta.getStatusCode());
	}

	@Test
	public void CP3_1_RutaGetBienFormadaAutomataNoEncontrado() {
		ResponseEntity<String> respuesta = template.getForEntity(createURLWithPort("/checkword/625/bade"),
				null);
		assertEquals(HttpStatus.INSUFFICIENT_STORAGE, respuesta.getStatusCode());
	}	

	@Test
	public void CP3_2_RutaGetIdAutomataNoNumerico() {
		ResponseEntity<String> respuesta = template.getForEntity(createURLWithPort("/checkword/deberiaSerUnNumero/bade"),
				null);
		assertEquals(HttpStatus.BAD_REQUEST, respuesta.getStatusCode());
	}
	
	@Test
	public void CP3_3_RutaPostBienFormadaAutomataDefinicionErroneaControlada() {
		ResponseEntity<String> respuesta = template.postForEntity(createURLWithPort("/generate"),
				"esto no es una definicion valida de primera linea", null, headers);
		assertEquals(HttpStatus.NOT_ACCEPTABLE, respuesta.getStatusCode());
	}	
	
	@Test
	public void CP4_GeneracionCorrecta() {
		ResponseEntity<?> respuesta = template.postForEntity(createURLWithPort("/generate"),
				EJEMPLO_BODY_GENERAR, null, headers);
		assertEquals(HttpStatus.CREATED, respuesta.getStatusCode());
	}
	
	@Test
	public void CP5_0_ValidarPalabraConAutomataGenerado() {

		MockHttpServletRequest request = new MockHttpServletRequest();
		request.setServerPort(port);
		request.setRequestURI(createURLWithPort("/generate"));
		request.setMethod(RequestMethod.POST.name());
		ResponseEntity<?> respuesta = automataController.generaAutomata(EJEMPLO_BODY_GENERAR, request);
		assertEquals(HttpStatus.CREATED, respuesta.getStatusCode());
		AutomataPila auto = (AutomataPila) respuesta.getBody();
		assertTrue(auto.getInicialPila() == 'S');
		assertTrue(auto.getEstadoInicial().equals("q0"));
		//-----------------------------------------------------
		request = new MockHttpServletRequest();
		request.setServerPort(port);
		request.setRequestURI(createURLWithPort("/checkword/"+auto.getIdAutomata()+"/ab"));
		request.setMethod(RequestMethod.GET.name());
		ResponseEntity<?> respuestaPalabra = automataController.validaPalabra(String.valueOf(auto.getIdAutomata()), "ab");
		assertTrue(respuestaPalabra.getStatusCode() == HttpStatus.OK);
	}
	
	@Test
	public void CP6_0_RechazarPalabraConAutomataGenerado() {

		MockHttpServletRequest request = new MockHttpServletRequest();
		request.setServerPort(port);
		request.setRequestURI(createURLWithPort("/generate"));
		request.setMethod(RequestMethod.POST.name());
		ResponseEntity<?> respuesta = automataController .generaAutomata(EJEMPLO_BODY_GENERAR, request);
		assertEquals(HttpStatus.CREATED, respuesta.getStatusCode());
		AutomataPila auto = (AutomataPila) respuesta.getBody();
		assertTrue(auto.getInicialPila() == 'S');
		assertTrue(auto.getEstadoInicial().equals("q0"));
		//-----------------------------------------------------
		request = new MockHttpServletRequest();
		request.setServerPort(port);
		request.setRequestURI(createURLWithPort("/checkword/"+auto.getIdAutomata()+"/ba"));
		request.setMethod(RequestMethod.GET.name());
		ResponseEntity<?> respuestaPalabra = automataController.validaPalabra(String.valueOf(auto.getIdAutomata()), "ba");
		assertTrue(respuestaPalabra.getStatusCode() == HttpStatus.OK);
	}
	
	@Test
	public void CP6_1_RechazarPalabraConAutomataGeneradoCharNoAceptado() {

		MockHttpServletRequest request = new MockHttpServletRequest();
		request.setServerPort(port);
		request.setRequestURI(createURLWithPort("/generate"));
		request.setMethod(RequestMethod.POST.name());
		ResponseEntity<?> respuesta = automataController .generaAutomata(EJEMPLO_BODY_GENERAR, request);
		assertEquals(HttpStatus.CREATED, respuesta.getStatusCode());
		AutomataPila auto = (AutomataPila) respuesta.getBody();
		assertTrue(auto.getInicialPila() == 'S');
		assertTrue(auto.getEstadoInicial().equals("q0"));
		//-----------------------------------------------------
		request = new MockHttpServletRequest();
		request.setServerPort(port);
		request.setRequestURI(createURLWithPort("/checkword/"+auto.getIdAutomata()+"/ez"));
		request.setMethod(RequestMethod.GET.name());
		ResponseEntity<?> respuestaPalabra = automataController.validaPalabra(String.valueOf(auto.getIdAutomata()), "ez");
		assertTrue(respuestaPalabra.getStatusCode() == HttpStatus.NOT_ACCEPTABLE);
	}	
	
	@Test
	public void CP7_0_CorrectorCaracteresEntradaBodyPOST(){
		String automataFormadoConEspeciales = "%7Ba%2Cb%7D%3B%7BS%2CA%2CB%7D%3B%7Bp%2Cq%2Cr%7D%3Bp%3BS%3B%0Af%28p%2Cb%2CS%29=%28p%2CBS%29%0Af%28p%2Cb%2CB%29%3D%28p%2CBB%29%0Af%28p%2C+%2CS%29%3D%28p%2C+%29%0Af%28p%2Cb%2CB%29%3D%28r%2C+%29%0Af%28p%2Ca%2CB%29%3D%28q%2C+%29%0Af%28q%2Ca%2CB%29%3D%28q%2C+%29%0Af%28q%2C+%2CS%29%3D%28q%2C+%29%0Af%28q%2Cb%2CB%29%3D%28r%2C+%29%0Af%28r%2Cb%2CB%29%3D%28r%2C+%29%0Af%28r%2C+%2CS%29%3D%28r%2C+%29";
		automataFormadoConEspeciales = Utils.correctorCharEspeciales(automataFormadoConEspeciales);
		assertTrue(!automataFormadoConEspeciales.contains("%"));
	}
	
	@Test
	public void CP8_0_VaciadoMemoriaPrincipalAutomatas(){
		MockHttpServletRequest request = new MockHttpServletRequest();
		request.setServerPort(port);
		request.setRequestURI(createURLWithPort("/generate"));
		request.setMethod(RequestMethod.POST.name());
		for (int i=0; i< Utils.TAMANIO_MAPA_AUTOMATAS_GENERADOS+1; i++) {
			ResponseEntity<?> respuesta = automataController.generaAutomata(EJEMPLO_BODY_GENERAR, request);
			assertEquals(HttpStatus.CREATED, respuesta.getStatusCode());
		}
		ResponseEntity<?> respuesta = automataController.generaAutomata(EJEMPLO_BODY_GENERAR, request);
		AutomataPila auto = (AutomataPila) respuesta.getBody();
		assertTrue(Utils.TAMANIO_MAPA_AUTOMATAS_GENERADOS > auto.getIdAutomata());
	}
	
	private String createURLWithPort(String uri) {
		return "http://localhost:" + port + uri;
	}
}
