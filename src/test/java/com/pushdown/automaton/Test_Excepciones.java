package com.pushdown.automaton;

import org.junit.Test;

import com.pushdown.automaton.exceptions.AlfabetoNoValidoException;
import com.pushdown.automaton.exceptions.DatosEntradaErroneosException;

public class Test_Excepciones {

	@Test (expected = AlfabetoNoValidoException.class)
	public void CP0_1excepcionesAlfabeto() throws AlfabetoNoValidoException{
		throw new AlfabetoNoValidoException("Es una prueba");
	}
	
	@Test (expected = AlfabetoNoValidoException.class)
	public void CP0_2excepcionesAlfabeto() throws AlfabetoNoValidoException{
		throw new AlfabetoNoValidoException("Es una prueba 2", new Throwable());
	}
	
	@Test (expected = DatosEntradaErroneosException.class)
	public void CP0_3DatosEntradaErroresException() throws DatosEntradaErroneosException{
		throw new DatosEntradaErroneosException("Es una prueba 3");
	}
	
	@Test (expected = DatosEntradaErroneosException.class)
	public void CP0_4DatosEntradaErroresException() throws DatosEntradaErroneosException{
		throw new DatosEntradaErroneosException("Es una prueba 3", new Throwable());
	}
}
